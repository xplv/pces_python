import numpy
import cv2
import resources
from enum import Enum


class State():
    """ Klasse um Zustände des Spiels zu beschreiben (z.B. Intro, Titel, Kampf, Pokegear, etc.). Jenachdem können dann
    durch Subklassen unterschiedliche Methoden zur Suche nach Templates verwendet werden, bzw. spezifische Resources
    über den aktuellen Frame gemalt werden.
    """

    # Pfade zu den jeweiligen Ordnern
    templatesPath = "templates/"
    resourcePath = "resources/"

    def __init__(self, templateFolder):
        """ init methode, ladet alle für den State relevanten Templates/Resourcen

        :param templateFolder: Name des Template Ordners, dieser bestimmt auch den Namen des States
        """
        self.name = templateFolder
        self._nextStates = []
        self.nextStateStrings = []

        templates = resources.loadTemplates(self.templatesPath + self.name)

        self.stateBefore = None

        self.templatesY = []
        self.templatesN = []
        for t in templates:
            if t.shouldBePresent:
                self.templatesY.append(t)
            else:
                self.templatesN.append(t)

        self.resources = resources.loadResources(self.resourcePath + self.name)

    def __str__(self):
        return self.name

    def change(self, stateBefore, currentFrame, frameBeforeChange):
        """ Methode die beim wechsel aufgerufen werden soll, es können hier state spezifische variablen gesetzt werden

        :param stateBefore: der state for dem wechsel
        :param frameBeforeChange: der frame vor dem wechsel
        :return: returned den state selbst, damit er auf den aktuellen gesetzt werden kann
        """

        self.stateBefore = stateBefore
        return  self

    def addNextState(self, state):
        if type(state) is list:
            for s in state:
                self._nextStates.append(s)
                self.nextStateStrings.append(s.name)

        else:
            self._nextStates.append(state)
            self.nextStateStrings.append(state.name)

    def detect(self, currentFrame):
        """ Base Method für die Detection

        :param currentFrame: numpy array des aktuellen Frames
        :return: Tupel mit (wasFound, x, y, width, height)
        """
        return False

    def draw(self, currentFrame, lastFrame):
        """ Base Method um den currentFrame mit dem gewünschten Content zu übermalen.

        :param currentFrame: numpy array des aktuellen Frames
        :return: gibt den neuen Frame zurück
        """
        return currentFrame

    def exchangeColor(self, img, colorOld=[255,255,255], colorNew=[0,0,0]):
        r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
        mask = (r == colorOld[0]) & (g == colorOld[1]) & (b == colorOld[2])
        img[:, :, 0:3][mask] = colorNew

    def exchangeEverythingButColor(self, img, colorOld=[255,255,255], colorNew=[0,0,0]):
        r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
        mask = (r != colorOld[0]) | (g != colorOld[1]) | (b != colorOld[2])
        img[:, :, 0:3][mask] =  colorNew


class StateTemplateMatching(State):
    """ Parent Class für alle States, welche Template Matching benötigen
    """
    def __init__(self, templateFolder):
        State.__init__(self, templateFolder)

    def detect(self, currentFrame):
        return False

    def draw(self, currentFrame, lastFrame):
        return currentFrame


class StateHistDiff(State):
    """ Parent Class für States, welche mit Histogramm Difference als Grundlage auskommen"""

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        """ init methode

        :param templateFolder: see State
        :param minimum_commutative_image_diff: Threshold für die Differenz
        """
        State.__init__(self, templateFolder)
        self.minimum_commutative_image_diff = minimum_commutative_image_diff

    @staticmethod
    def createCutout(currentFrame, template):
        """ Schneidet bei currentFrame alles weg, was bei templateList Transparent ist, damit nur relevante Bereiche
        verglichen werden

        :param currentFrame: numpy array des aktuellen Frames
        :param template: template nach welchem im ausgeschnitt gesucht werden soll
        :return: ein numpy array mit dem zurechtgeschnittenen currentFrame
        """
        cutout = numpy.copy(currentFrame)
        cutout[template.maskTransparent] = [0,0,0,0]
        return cutout

    def detect(self, currentFrame):
        """ Detected anhand eines Histogrammvergleiches

        :param currentFrame: see State
        :return: see State ( x, y, width und height sind immer 0, da diese hier irrelevant sind)
        """

        for t in self.templatesN:
            cutoutN = StateHistDiff.createCutout(currentFrame, t)
            commutative_image_diff = self.get_image_difference(cutoutN, t.img)
            if commutative_image_diff <= self.minimum_commutative_image_diff:
                return False


        for t in self.templatesY:
            cutoutY = StateHistDiff.createCutout(currentFrame, t)
            commutative_image_diff = self.get_image_difference(cutoutY, t.img)
            if commutative_image_diff <= self.minimum_commutative_image_diff:
                return True

        return False


    def get_image_difference(self, image_1, image_2):
        """ Histogramm vergleich beider numpy arrays

        :param image_1: numpy array 1
        :param image_2: numpy array 2
        :return: differnez (ich glaub 0 < x < 1)
        """
        first_image_hist = cv2.calcHist([image_1], [0], None, [256], [0, 256])
        second_image_hist = cv2.calcHist([image_2], [0], None, [256], [0, 256])

        img_hist_diff = cv2.compareHist(first_image_hist, second_image_hist, cv2.HISTCMP_BHATTACHARYYA)

        return img_hist_diff


class StateMenu(StateHistDiff):
    class MenuEnum(Enum):
        POKEDEX = 0
        POKEMON = 1
        PACK = 2
        POKEGEAR = 3
        TRAINER = 4
        SAVE = 5
        OPTION = 6
        EXIT = 7

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.selectedItem = StateMenu.MenuEnum.POKEDEX
        self.lastFreeplayFrame = None
        self.arrowTemplate =  None
        self.menuImages = [None] * 8

        for r in self.resources:
            if r.name == "0_selected":
                self.menuImages[0] = r
            elif r.name == "1_selected":
                self.menuImages[1] = r
            elif r.name == "2_selected":
                self.menuImages[2] = r
            elif r.name == "3_selected":
                self.menuImages[3] = r
            elif r.name == "4_selected":
                self.menuImages[4] = r
            elif r.name == "5_selected":
                self.menuImages[5] = r
            elif r.name == "6_selected":
                self.menuImages[6] = r
            elif r.name == "7_selected":
                self.menuImages[7] = r
            elif r.name == "8_selected":
                self.menuImages[8] = r
            elif r.name == "arrow":
                self.arrowTemplate = r

    def change(self, stateBefore, currentFrame, frameBeforeChange):
        """ Methode die beim wechsel aufgerufen werden soll, es können hier state spezifische variablen gesetzt werden

        :param stateBefore: der state for dem wechsel
        :param frameBeforeChange: der frame vor dem wechsel
        :return: returned den state selbst, damit er auf den aktuellen gesetzt werden kann
        """
        self.stateBefore = stateBefore

        if self.stateBefore.name == "FREEPLAY":
            self.lastFreeplayFrame = frameBeforeChange

        return self

    def draw(self, currentFrame, lastFrame):
        # sollte nur der fall sein wenn man vom menu aus startet
        if self.lastFreeplayFrame is None:
            return currentFrame
        elif not self.detect(currentFrame):
            return currentFrame
        elif not self.getSelectedItem(currentFrame):
            return lastFrame
        else:
            menuOverlay = self.menuImages[self.selectedItem.value]

            copy = numpy.copy(self.lastFreeplayFrame)

            copy[menuOverlay.maskNonTransparent] = [0, 0, 0, 0]
            copy += menuOverlay.img

            return copy

    def getSelectedItem(self, currentFrame):
        methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

        # TODO: Test which method is the best one
        method = eval(methods[0])

        res = cv2.matchTemplate(currentFrame, self.arrowTemplate.img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if max_val < 10000000:
            return False

        if max_loc[1] > 0 and max_loc[1] < 288:
            self.selectedItem = self.calcMenuItemPosition(max_loc[1])
            return True

        return False

    def calcMenuItemPosition(self, y):
        # die größe eines Menüeintrags ist bei 2x Framegröße 32 pixel
        if y == 30:
            return StateMenu.MenuEnum(0)
        else:
            position =  round((y-30)/32)
            return StateMenu.MenuEnum(position)

class StateFight(StateHistDiff):
    allPokemon = None
    myPokemon = None
    enemyPokemon = None

    def __init__(self, templateFolder, minimum_commutative_image_diff, allPokemon):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        if StateFight.allPokemon == None:
            StateFight.allPokemon = allPokemon

    def editImage(self, image):
        image = resources.cropImage(image)
        self.exchangeEverythingButColor(image, [255, 255, 255], [0, 0, 0])




        return image

    def change(self, stateBefore, currentFrame, frameBeforeChange):
        if stateBefore.name == "UNSURE":
            #[y,x]
            myPokemonImg = numpy.copy(currentFrame[63:198, 4:144, :])
            enemyPokemonImg = numpy.copy(currentFrame[0:112, 176:320, :])

            myPokemonImg = self.editImage(myPokemonImg)
            enemyPokemonImg = self.editImage(enemyPokemonImg)

            myPokemonFound = False
            enemyPokemonFound = False
            for key in StateFight.allPokemon:
                if myPokemonFound and enemyPokemonFound:
                    break
                else:
                    p = StateFight.allPokemon[key]
                    if not myPokemonFound and p.img_back.shape == myPokemonImg.shape:
                        if self.get_hist_difference(myPokemonImg, p.hist_back) <= self.minimum_commutative_image_diff:
                            StateFight.myPokemon = p
                            myPokemonFound = True

                    if not enemyPokemonFound and p.img_front.shape == enemyPokemonImg.shape:
                        if self.get_hist_difference(enemyPokemonImg, p.hist_front) <= self.minimum_commutative_image_diff:
                            StateFight.enemyPokemon = p
                            enemyPokemonFound = True

        return self

    def draw(self, currentFrame, lastFrame):
        if StateFight.myPokemon == None and StateFight.enemyPokemon == None:
            return currentFrame
        else:
            if StateFight.myPokemon != None:
                currentFrame[128:142, 160:174,:] = StateFight.myPokemon.type1.img
                if StateFight.myPokemon.type2 != None:
                    currentFrame[128:142, 176:190, :] = StateFight.myPokemon.type2.img

            if StateFight.enemyPokemon != None:
                currentFrame[16:30, 32:46,:] = StateFight.enemyPokemon.type1.img
                if StateFight.enemyPokemon.type2 != None:
                    currentFrame[16:30, 48:62 :] = StateFight.enemyPokemon.type2.img

            return currentFrame

    def get_hist_difference(self, img, hist):
        first_image_hist = cv2.calcHist([img], [0], None, [256], [0, 256])
        second_image_hist = hist

        img_hist_diff = cv2.compareHist(first_image_hist, second_image_hist, cv2.HISTCMP_BHATTACHARYYA)

        return img_hist_diff


class StateFightMenu(StateFight):
    def change(self, stateBefore, currentFrame, frameBeforeChange):
        StateFight.change(self,stateBefore,currentFrame,frameBeforeChange)
        return self

    def draw(self, currentFrame, lastFrame):
        StateFight.draw(self,currentFrame,lastFrame)
        expBar = currentFrame[182:188, 160:288, :]
        expBar = numpy.fliplr(expBar)

        currentFrame[182:188, 160:288, :] = expBar
        return currentFrame

class StateFightAttacked(StateFight):
    def change(self, stateBefore, currentFrame, frameBeforeChange):
        StateFight.change(self, stateBefore, currentFrame, frameBeforeChange)

        if stateBefore.name == "FIGHT_ATTACKS":
            stateBefore.attacked()

        return self

    def draw(self, currentFrame, lastFrame):
        expBar = currentFrame[182:188, 160:288, :]
        expBar = numpy.fliplr(expBar)

        currentFrame[182:188, 160:288, :] = expBar
        return currentFrame

class StateFightWon(StateFight):
    def draw(self, currentFrame, lastFrame):
        expBar = currentFrame[182:188, 160:288, :]
        expBar = numpy.fliplr(expBar)

        currentFrame[182:188, 160:288, :] = expBar
        return  currentFrame

class StateFightAttacks(StateFight):
    def __init__(self, templateFolder, minimum_commutative_image_diff, allPokemon):
        StateFight.__init__(self, templateFolder, minimum_commutative_image_diff, allPokemon)
        self.selectedItem = 0
        self.lastFightingMenuFrame = None
        self.arrowTemplate = None
        self.pokemonMask = None
        self.pokemonImage = None
        self.expBar = None
        self.attackImagesOriginal = [None] * 4
        self.attackImagesNew = [None] * 4
        self.attacks = [None] * 4
        self.numbers = [None] * 10
        self.emptyRow = None
        self.emptyNumber = None
        self.bestSymbol = None

        self.selectionColor = [230, 217, 136]

        for r in self.resources:
            if r.name == "arrow":
                self.arrowTemplate = r
            elif r.name == "pokemon_mask":
                self.pokemonField = r
            elif r.name == "emptyRow":
                self.emptyRow = r.img
            elif r.name == "emptyNumber":
                self.emptyNumber = r.img
            elif r.name == "best":
                self.bestSymbol = r.img
            else:
                try:
                    self.numbers[int(r.name)] = r.img
                except:
                    pass

    def attacked(self):
        if StateFight.myPokemon != None and StateFight.myPokemon.attacks[self.selectedItem] != None:
            StateFight.myPokemon.attacks[self.selectedItem].curPP -= 1

    def change(self, stateBefore, currentFrame, frameBeforeChange):
        """ Methode die beim wechsel aufgerufen werden soll, es können hier state spezifische variablen gesetzt werden

        :param stateBefore: der state for dem wechsel
        :param frameBeforeChange: der frame vor dem wechsel
        :return: returned den state selbst, damit er auf den aktuellen gesetzt werden kann
        """
        StateFight.change(self,stateBefore,currentFrame,frameBeforeChange)
        self.stateBefore = stateBefore

        if self.stateBefore.name == "FIGHT_MENU":
            self.expBar = numpy.copy(frameBeforeChange[182:188, 160:288, :])
            self.lastFightingMenuFrame = frameBeforeChange
            frameBeforeChange[self.pokemonField.maskTransparent] = [0,0,0,0]
            self.pokemonImage = frameBeforeChange
        return self

    def draw(self, currentFrame, lastFrame):
        lastFrame = StateFight.draw(self, currentFrame, lastFrame)
        if type(self.expBar) != type(None):
            currentFrame[182:188, 160:288, :] = self.expBar

        # sollte nur der fall sein wenn man vom menu aus startet
        if self.lastFightingMenuFrame is None:
            return currentFrame
        elif not self.detect(currentFrame):
            return currentFrame
        elif not self.getSelectedItem(currentFrame):
            return lastFrame
        else:
            currentFrame = StateFight.draw(self, currentFrame, lastFrame)

            currentFrame[self.pokemonField.maskNonTransparent] = [0,0,0,0]
            currentFrame += self.pokemonImage

            attacksOriginal = [None] * 4
            attacksOriginal[0] = currentFrame[208:222, 96:310, :]
            attacksOriginal[1] = currentFrame[224:238, 96:310, :]
            attacksOriginal[2] = currentFrame[240:254, 96:310, :]
            attacksOriginal[3] = currentFrame[256:270, 96:310, :]

            attacksNew = [None] * 4

            for i in range(0,4):
                attacksNew[i] = numpy.copy(self.emptyRow)
                attacksNew[i][1:15, 6:220, :] = attacksOriginal[i]

                if StateFight.myPokemon != None and StateFight.myPokemon.attacks[i] != None:
                    attacksNew[i][2:14, 266:294, :] = self.buildNumber(StateFight.myPokemon.attacks[i].curPP, StateFight.myPokemon.attacks[i].maxPP)
                    attacksNew[i][1:15, 246:260, :] = StateFight.myPokemon.attacks[i].type.img
                    if StateFight.myPokemon.attacks[i].isBest:
                        attacksNew[i][1:15, 226:240, :] = self.bestSymbol

            self.exchangeColor(attacksNew[self.selectedItem],[255,255,255], self.selectionColor)

            currentFrame[207:223, 10:310, :] = attacksNew[0]
            currentFrame[223:239, 10:310, :] = attacksNew[1]
            currentFrame[239:255, 10:310, :] = attacksNew[2]
            currentFrame[255:271, 10:310, :] = attacksNew[3]

            return currentFrame

    def buildNumber(self, numberNow, numberMax):
        if numberNow / 99 > 1:
            numberNow = 99

        number = numpy.copy(self.emptyNumber)
        numStr = str(numberNow)

        if len(numStr) == 1:
            number[0:12, 14:28, :] = self.numbers[int(numStr[0])]
        else:
            number[0:12 , 0:14, :] = self.numbers[int(numStr[0])]
            number[0:12, 14:28, :] = self.numbers[int(numStr[1])]

        if numberNow/numberMax < 0.25:
            self.exchangeColor(number, [0,0,0], [0,0,255])
        elif numberNow/numberMax < 0.5:
            self.exchangeColor(number, [0,0,0], [40, 119, 231])

        return number

    def getSelectedItem(self, currentFrame):
        methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

        # TODO: Test which method is the best one
        method = eval(methods[0])

        res = cv2.matchTemplate(currentFrame, self.arrowTemplate.img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if max_val < 10000000:
            return False

        if max_loc[1] > 0 and max_loc[1] < 288:
            self.selectedItem = self.calcMenuItemPosition(max_loc[1])
            return True

        return False

    def calcMenuItemPosition(self, y):
        # die größe eines Menüeintrags ist bei 2x Framegröße 32 pixel
        if y == 208:
            return 0
        else:
            return round((y-208)/16)

class StateTitle(StateHistDiff):
    """ State für den Title Screen """

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.overlay = None

        for res in self.resources:
            if res.name == "extension_suit_logo":
                self.overlay = res

    def draw(self, currentFrame, lastFrame):
        """ Zeichnet das Logo auf den Titelbildschirm

        :param currentFrame: see StateHistDiff
        :param matchingInfo: see StateHistDiff
        """

        currentFrame[self.overlay.maskNonTransparent] = [0,0,0,0]
        currentFrame += self.overlay.img

        return currentFrame

class StatePokeGearStart(StateHistDiff):
    """ State für den PokeGear Start Screen """

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.overlay = None

        for r in self.resources:
            if r.name == "day_care_status":
                self.overlay = r
            elif r.name == "phone_icon":
                self.phone_icon = r


    def draw(self, currentFrame, lastFrame):
        currentFrame[self.overlay.maskNonTransparent + self.phone_icon.maskNonTransparent] = [0, 0, 0, 0]
        currentFrame += self.overlay.img + self.phone_icon.img

        return currentFrame

class StatePokeGearRadio(StateHistDiff):
    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.phone_icon = None

        for r in self.resources:
            if r.name == "phone_icon":
                self.phone_icon = r

    def draw(self, currentFrame, lastFrame):
        currentFrame[self.phone_icon.maskNonTransparent] = [0, 0, 0, 0]
        currentFrame += self.phone_icon.img

        return currentFrame

class StatePokeGearPhone(StateHistDiff):
    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.selectedItem = 0
        self.lastFreeplayFrame = None
        self.arrowTemplate = None
        self.images = [None] * 4

        for r in self.resources:
            if r.name == "1":
                self.images[0] = r
            elif r.name == "2":
                self.images[1] = r
            elif r.name == "3":
                self.images[2] = r
            elif r.name == "4":
                self.images[3] = r
            elif r.name == "arrow":
                self.arrowTemplate = r.img

    def draw(self, currentFrame, lastFrame):
        if not self.detect(currentFrame):
            return currentFrame
        elif not self.getSelectedItem(currentFrame):
            return lastFrame
        else:
            overlay = self.images[self.selectedItem]
            currentFrame[overlay.maskNonTransparent] = [0, 0, 0, 0]
            currentFrame += overlay.img

            return currentFrame

    def getSelectedItem(self, currentFrame):
        methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

        # TODO: Test which method is the best one
        method = eval(methods[0])

        res = cv2.matchTemplate(currentFrame, self.arrowTemplate, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if max_val < 4000000:
            return False

        if max_loc[1] > 0 and max_loc[1] < 288:
            self.selectedItem = self.calcMenuItemPosition(max_loc[1])
            return True

        return False

    def calcMenuItemPosition(self, y):
        # die größe eines Menüeintrags ist bei 2x Framegröße 32 pixel
        if y == 64:
            return 0
        else:
            position =  round((y-64)/32)
            return position

class StatePokeGearMap(StateHistDiff):
    """ State für den PokeGearMap Screen """

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.overlay = None
        self.phone_icon = None

        for r in self.resources:
            if r.name == "berry_map":
                self.overlay = r
            elif r.name == "phone_icon":
                self.phone_icon = r


    def draw(self, currentFrame, lastFrame):
        currentFrame[self.overlay.maskNonTransparent + self.phone_icon.maskNonTransparent] = [0, 0, 0, 0]
        currentFrame += self.overlay.img + self.phone_icon.img

        return currentFrame

class StatePackBalls(StateHistDiff):
    """ State für den Pack Balls Screen """

    def __init__(self, templateFolder, minimum_commutative_image_diff):
        StateHistDiff.__init__(self, templateFolder, minimum_commutative_image_diff)
        self.overlay = None

        for r in self.resources:
            if r.name == "catch_chance":
                self.overlay = r


    def draw(self, currentFrame, lastFrame):
        currentFrame[self.overlay.maskNonTransparent] = [0, 0, 0, 0]
        currentFrame += self.overlay.img

        return currentFrame
