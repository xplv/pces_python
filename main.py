#!/usr/local/bin/python
# coding: latin-1

import sys
import time
import numpy
import cv2
from pces import PCES
import mss
import mss.tools

"""
#############################################################
# -------------------- Voraussetzungen -------------------- #
#############################################################

- (Arch) Linux (K�nnte auch auf anderen funktionieren, noch nicht getestet)

- Ben�tigte Libraries (via pip):
-- virtualenv (optional but preferred)
-- mss
-- numpy
-- opencv-python

- Im Emulator FrameSize auf x2

<<<<<<< Updated upstream
- Namenskonvention für templates:
=======
- Bei Bildern in "resources" (also welche zum �bermalen verwendet werden) m�ssen die transparenten Fl�chen
  die Werte (0,0,0,0) haben! Daf�r z.B. in GIMP einen neuen Layer als untesten hinzuf�gen, diesen schwarz machen, die
  Opacity auf 0% stellen und dann die Layer Mergen (funktioniert zumindest bei mir so)
  
- Namenskonvention f�r templates:
>>>>>>> Stashed changes
-- mit y beginnend -> template kann (!) im Bild enhalten sein
-- mit n beginnend -> template darf NICHT im Bild enthalten sein
-- dannach immer "-", ansonsten keine!
-- nicht mehr als ein "." und zwar bei der Dateiendung!

-- pngs as resources only!

-- In Pokemon selbst: FRAME TYPE 6!
"""
class Debug():
    FLAGS = {
        'PRINT_FPS': 0,
        'PRINT_STATE_CHANGE': 1,
        'PRINT_INITIAL_STATE': 1
    }

if __name__ == "__main__":
    pces = PCES(("Pokemon", "mGBA"))
    running = True

    # Automatically determine starting state
    initState = None
    currentFrame = numpy.array(mss.mss().grab(pces.area))
    for state in pces.game.states:
        if state.detect(currentFrame):
            initState = state
            break

    if initState is None:
        print("WARNING: initState not detected, is set to UNSURE")
        initState = pces.game.states[0]

    pces.game.currentState = initState

    if Debug.FLAGS['PRINT_INITIAL_STATE']:
        print("initial state: " + pces.game.currentState.name, "... looking for:", pces.game.currentState.nextStateStrings)

    lastFrame = numpy.array(mss.mss().grab(pces.area))
    secondLastFrame = numpy.array(mss.mss().grab(pces.area))

    cv2.namedWindow('Pokemon Crystal Extension Suit', 16)
    cv2.resizeWindow('Pokemon Crystal Extension Suit', 320, 288)
    while running:
        with mss.mss() as sct:
            last_time = time.time()

            """always check two Frames, so the flickering doesnt have an impact"""
            # Get raw pixels from the screen, save it to a Numpy array
            currentFrame = numpy.array(sct.grab(pces.area))

            expectedState = None
            # First check
            for state in pces.game.currentState._nextStates:
                if state.detect(currentFrame):
                    expectedState = state
                    break

            # Confirming state change with second check
            if expectedState != None:
                checkFrame = numpy.array(sct.grab(pces.area))

                if expectedState.detect(checkFrame):
                    pces.game.currentState = expectedState.change(pces.game.currentState, currentFrame, secondLastFrame)

                    if Debug.FLAGS['PRINT_STATE_CHANGE']:
                        print("STATE NOW:", expectedState.name, "... looking for:", expectedState.nextStateStrings)


            # do the drawing
            newFrame = pces.game.currentState.draw(currentFrame, secondLastFrame)

            cv2.imshow('Pokemon Crystal Extension Suit', newFrame)
            cv2.waitKey(1)


            if Debug.FLAGS['PRINT_FPS']:
                print('fps: {0}'.format(1 / (time.time()-last_time)))


            secondLastFrame = lastFrame
            lastFrame = newFrame

