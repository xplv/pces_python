from PIL import Image
import glob
import numpy
import resources

def cut(dir):
    processing = 0

    allFiles = glob.glob(dir + "*.png")
    for file in allFiles:
        print("Processing Image " + str(processing) + "/" + str(len(allFiles)))

        img = Image.open(file)
        img.load()

        cropped = Image.fromarray(resources.cropImage(img))

        a_channel = Image.new('L', cropped.size, 255)  # 'L' 8-bit pixels, black and white
        cropped.putalpha(a_channel)
        cropped.save(file)

        processing += 1

cut("/home/yanik/gitrepos/pces_python/resources/_POKEMON_FRONT/")
cut("/home/yanik/gitrepos/pces_python/resources/_POKEMON_BACK/")