import glob, os
import requests
from io import BytesIO
from PIL import Image
import numpy
import resources

pokemon = [str] * 252
pokemon[0] = ''

list = open("../resources/_pokemon_list", "r")
i = 1
for line in iter(list):
    pokemon[i] = line.split("\n")[0].split(" ")[1].lower()
    i += 1

list.close()

def processImage(pathFrom, pathTo):
    '''
    Iterate the GIF, extracting each frame.
    '''
    downloading = 1
    warnings = []
    for name in pokemon:
        if name == '':
            continue

        print("Downloading " + str(downloading) + "/" + str(len(pokemon) -1))
        for w in warnings:
            print(w)



        url = pathFrom + name + ".png"
        response = requests.get(url)


        if response.status_code == 404:
            warnings.append("WARNING: No image for " + name + "!")
            continue

        img = Image.open(BytesIO(response.content))
        img.load()
        img = img.resize((112,112), Image.NONE)

        cropped = Image.fromarray(resources.cropImage(img))
        a_channel = Image.new('L', cropped.size, 255)  # 'L' 8-bit pixels, black and white
        cropped.putalpha(a_channel)

        cropped.save(pathTo + name.upper() + ".png")

        downloading += 1


processImage("https://img.pokemondb.net/sprites/crystal/normal/", "/home/yanik/gitrepos/pces_python/resources/_POKEMON_FRONT/")
processImage('https://img.pokemondb.net/sprites/crystal/back-normal/', "/home/yanik/gitrepos/pces_python/resources/_POKEMON_BACK/")

