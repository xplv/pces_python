from pces import PCES
import mss
import mss.tools

""" Die Klasse ist dazu da um schnell Screenshots vom Emulator zur machen z.B. für die Erstellung von Templates
"""
area = PCES.findWindowLocation(("mgba","Pokemon"))

def screenshot(area):
    with mss.mss() as sct:
        # The screen part to capture
        output = 'templates/sct-{top}x{left}_{width}x{height}.png'.format(**area)

        # Grab the data
        sct_img = sct.grab(area)

        # Save to the picture file
        mss.tools.to_png(sct_img.rgb, sct_img.size, output)

screenshot(area)