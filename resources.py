import cv2
from os import walk
import numpy

class Resource():
    """ Eine Resource ist ein Bild, welches zum übermalen verwendet wird.
    """

    def __init__(self, name, fileLocation):
        """ init methode, ließt das Bild als numpy array ein

        :param name: name der Resource, dient innerhalb der States zur identifizierung
        :param fileLocation: relativer Pfad zum File, ausgehend von dem Projekt Ordner (denke ich, könnte auch
                             von state.py sein)
        """
        self.name = name
        self.fileLocation = fileLocation

        self.img = cv2.imread(self.fileLocation, cv2.IMREAD_UNCHANGED)

        try:
            self.maskNonTransparent = self.img[:,:,3] != 0
            self.maskTransparent = self.img[:,:,3] == 0

            # fix for when transparent isnt exactly [0,0,0,0]
            self.img[self.maskTransparent ] = [0,0,0,0]
        except IndexError:
            print("ERROR, Resource for " + self.name + " in " + self.fileLocation + " has no Alphachannel!")
            exit(-1)

    def __str__(self):
        return self.name

class Template(Resource):
    """ Untertyp von Resource (für Code reusing), erweitert mit der Möglichkeit anzugeben, ob das Template im Frame
    vorhanden sein muss oder nicht"""


    def __init__(self, name, fileLocation, shouldBePresent):
        """ Es wird eine Maske erstellt, wo jeder Pixel "True" ist, der im img Transparent ist

        :param name: see class Resource
        :param fileLocation: see class Resource
        :param shouldBePresent: True für, das Template muss im Bild vorhanden sein, False für es darf nicht vorhanden sein
        """
        Resource.__init__(self,name,fileLocation)
        self.shouldBePresent = shouldBePresent

def getFiles(folder):
    """ Liefert eine Liste aller Files in dem übergebenen Ordner

    :param folder: Pfad zum Ordner von welchem der Inhalt aufgelistet werden soll
    :return: List mit allen Dateinamen als Strings
    """
    files = []
    for (dirpath, dirnames, filenames) in walk(folder):
        files.extend(filenames)
        break
    return files

def loadTemplates(folder):
    """ Laded alle Templates aus dem angegebenen Folder. Wenn ein Tempmlate keinen Prefix hat, wird shouldBePresent
    als True angenommen.

    :param folder: Pfad zum Ordner, von welchem die Templates geladen werden sollen
    :return: List mit Template Objects
    """
    files = getFiles(folder)

    if len(files) == 0:
        print("WARNING: No Templates found in ", folder)

    result = []

    for f in files:
        shouldBePresent = True
        # Name without y- or n- prefix
        splittedname = f.split("-")

        # Check ob es prefix gibt
        if len(splittedname) > 1:
            if splittedname[0] == "n":
                shouldBePresent = False
            # Name without .png
            pureName = splittedname[1].split(".")[0:len(splittedname) - 1][0]
        else:
            pureName = splittedname[0].split(".")[0:len(splittedname) - 1][0]

        t = Template(pureName, folder + "/" + f, shouldBePresent)
        result.append(t)

    return result

def loadResources(folder):
    """ Laded alle Resources aus dem angegebenen Folder

    :param folder: Pfad zum Ordner, von welchem die Resources geladen werden sollen
    :return: List mit Resources Objects
    """
    files = getFiles(folder)
    result = []

    for f in files:
        splittedname = f.split(".")

        if splittedname[1] == "png":
            pureName = splittedname[0]

            r = Resource(pureName, folder + "/" + f)
            result.append(r)

    return result

def cropImage(img):
    pix = numpy.array(img)

    #fix transparency
    mask = pix[:,:,3] == 0
    pix[:,:,3][mask] = 255

    cropBox = [0, pix.shape[1], 0, pix.shape[0]]
    image_data_bw = pix.mean(axis=2)
    non_empty_columns = numpy.where(image_data_bw.min(axis=0) != 255)[0]
    non_empty_rows = numpy.where(image_data_bw.min(axis=1) != 255)[0]
    if len(non_empty_columns) != 0:
        cropBox[2] = min(non_empty_columns)
        cropBox[3] = max(non_empty_columns)
    if len(non_empty_rows) != 0:
        cropBox[0] = min(non_empty_rows)
        cropBox[1] = max(non_empty_rows)

    cropped = img
    if len(non_empty_columns) != 0 or len(non_empty_rows) != 0 and \
            (cropBox[1] + 1) != pix.shape[1] and (cropBox[3] + 1) != pix.shape[0]:
        cropped = pix[cropBox[0]:cropBox[1] + 1, cropBox[2]:cropBox[3] + 1, :]

    return cropped
