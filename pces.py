import subprocess
import re
import sys
from game import Game

class PCES():
    """ Die Klasse um die Aufgaben der Extension Suite zusammenzuführen, enthält Methoden welche für mehrere Klassen
    relevant sein können.
    """

    def __init__(self, searchStrings):
        """ init methode, setzt die area in der die Screenshots gemacht werden, erstellt ein neues Game.

        :param searchStrings: serachStrings ist ein mit Strings gefülltes Tupel, welches verwendet wird um in der Fensterliste nach dem
        Emulator zu suchen.
        """
        self.searchStrings = searchStrings

        self.area = PCES.findWindowLocation(self.searchStrings)
        self.game = Game()


    @staticmethod
    def isLinux():
        """ Überprüft ob das OS Linux basierend ist

        :return: True für Linux, False für nicht
        """
        if sys.platform == 'linux':
            return True
        return False

    @staticmethod
    def isWindows():
        """ Überprüft ob das OS Windows ist

        :return: True für Windows, False für nicht
        """
        if sys.platform == 'win32':
            return True
        return False

    @staticmethod
    def findWindowLocation(searchStrings):
        """ Probiert die Position und Größe des Emulatorfensters anhand der searchStrings zu finden. Falls es mehrere
        Treffer gibt, wird das Programm abgebrochen!

        :param searchStrings: ein tupel, welches mehrere Strings enthalten darf
        :return: ein dict mit dem Aufbau {'top': top, 'left': left, 'width': width, 'height': height}, wobei top der
                 Abstand vom oberen Fensterrand (also die y Koordinate) und left der von linken Fensterrand (also x) ist.
        """

        result = []

        if PCES.isLinux():
            # liefert eine Auflistung aller Fenster mit Position und Größe
            windowList = subprocess.check_output(['xwininfo', '-root', '-tree']).decode('utf-8').split('\n')

            resultStrings = []

            for resultString in windowList:
                everySearchStringInResult = True

                for searchString in searchStrings:
                    if searchString.lower() not in resultString.lower():
                        everySearchStringInResult = False

                if everySearchStringInResult:
                    resultStrings.append(resultString)

            # filtert die relevanten Infos (y, x, width, height) aus dem String heraus
            for s in resultStrings:
                # für die Größe des Fensters z.B. "[...] 10x10+10+10 [...]"
                regexSize = re.search(r'\d*x\d*\+\d*\+\d*', s)
                matchSize = s[regexSize.start():regexSize.end()].split("+")[0].split("x")

                # für die Position des Fensters z.B. "[...] +10+10 [...]"
                regexPos = re.search(r'\s\+\d*\+\d*', s)
                matchPos = s[regexPos.start():regexPos.end()].split("+")

                # da wir von einer fixen höhe von 288 ausgehen, kann der top margin automatisch ermittelt werden
                margin_top = int(matchSize[1]) - 288

                x = int(matchPos[1])
                y = int(matchPos[2]) + margin_top
                width = int(matchSize[0])
                height = int(matchSize[1]) - margin_top

                result.append({'top': y, 'left': x, 'width': width, 'height': height})

        elif PCES.isWindows():
            # TODO: implement same funtionality for Windows
            pass


        if len(result) == 0:
            print("ERROR: None results for window!")
            exit(-1)

        if len(result) > 1:
            print("WARNING: More than one result, taking first one!")


        if result[0]['width'] != 320 or result[0]['height'] != 288:
            print("ERROR: Size is ", result[0]['width'], "x", result[0]['height'], " not 320x288")
            exit(-1)

        return result[0]

