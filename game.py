from state import StateHistDiff, StateTitle, StateMenu, StatePokeGearStart, StateFightAttacks, StateFightMenu, StateFightAttacked, StateFightWon, StatePackBalls, StatePokeGearMap, StatePokeGearPhone, StatePokeGearRadio
import resources
from enum import Enum, auto
import cv2

class Type():
    def __init__(self, name, img):
        self.name = name
        self.img = img
    def __str__(self):
        return self.name

class Attack():
    def  __init__(self, name, type, accuracy, curPP, maxPP, isBest):
        self.name = name
        self.type = type
        self.accuracy = accuracy
        self.curPP = curPP
        self.maxPP = maxPP
        self.isBest = isBest

    def __str__(self):
        return self.name

class Pokemon():
    def __init__(self, name, type1, type2, img_front, img_back):
        self.name = name
        self.type1 = type1
        self.type2 = type2
        self.attacks = [None] * 4

        frontR, frontG, frontB = img_front[:, :, 0], img_front[:, :, 1], img_front[:, :, 2]
        frontMask = (frontR != 255) | (frontG != 255) | (frontB != 255)
        img_front[:, :, 0:3][frontMask] = [0, 0, 0]

        backR, backG, backB = img_back[:, :, 0], img_back[:, :, 1], img_back[:, :, 2]
        backMask = (backR!= 255) | (backG != 255) | (backB != 255)
        img_back[:, :, 0:3][backMask] = [0, 0, 0]

        self.img_front = img_front
        self.img_back = img_back
        self.hist_front = cv2.calcHist([self.img_front], [0], None, [256], [0, 256])
        self.hist_back = cv2.calcHist([self.img_back], [0], None, [256], [0, 256])

class Game():
    """Diese Klasse ist zum verwalten für Infos über das Spiel selbst, also z.B. in welchem State es sich gerade befindet.

    Für die Zukunft geplant: z.B. welche Pokemon hat der Trainer
    """
    def __init__(self):
        self.currentState = None
        self.states = []
        self.attacks = []
        self.types = {}
        self.allPokemon = {}
        self.trainerPokemon = [None] * 6

        self.initializeTypes()
        self.initializeAllPokemon()
        self.initializeTrainerPokemon()
        self.initializeStates()

    def initializeTrainerPokemon(self):
        self.trainerPokemon[0] = self.allPokemon['PIKACHU']
        self.trainerPokemon[0].attacks[0] = Attack("SING", self.types['NORMAL'], 55, 5, 24, False)
        self.trainerPokemon[0].attacks[1] = Attack("THUNDERBOLT", self.types['ELECTRIC'], 100, 10, 24, True)
        self.trainerPokemon[0].attacks[2] = Attack("SURF", self.types['WATER'], 100, 21, 24, False)
        self.trainerPokemon[0].attacks[3] = Attack("HIDDEN POWER", self.types['NORMAL'], 100, 24, 24, False)

        self.trainerPokemon[1] = self.allPokemon['MACHAMP']
        self.trainerPokemon[1].attacks[0] = Attack("CROSS CHOP", self.types['FIGHTING'], 80, 8, 8, True)
        self.trainerPokemon[1].attacks[1] = Attack("CURSE", self.types['GHOST'], None, 16, 16, False)
        self.trainerPokemon[1].attacks[2] = Attack("ROCK SLIDE", self.types['ROCK'], 90, 16, 16, False)
        self.trainerPokemon[1].attacks[3] = Attack("HIDDEN POWER", self.types['NORMAL'], 100, 24, 24, False)

        self.trainerPokemon[2] = self.allPokemon['ARCANINE']
        self.trainerPokemon[2].attacks[0] = Attack("FIRE BLAST", self.types['FIRE'], 85, 8, 8, True)
        self.trainerPokemon[2].attacks[1] = Attack("HIDDEN POWER", self.types['NORMAL'], 100, 24, 24, False)
        self.trainerPokemon[2].attacks[2] = Attack("REST", self.types['PSYCHIC'], None, 16, 16, False)
        self.trainerPokemon[2].attacks[3] = Attack("SLEEP TALK", self.types['NORMAL'], None, 16, 16, False)

        self.trainerPokemon[2] = self.allPokemon['GENGAR']
        self.trainerPokemon[2].attacks[0] = Attack("THUDNER", self.types['ELECTRIC'], 70, 16, 16, False)
        self.trainerPokemon[2].attacks[1] = Attack("ICE PUNCH", self.types['ICE'], 100, 24, 24, False)
        self.trainerPokemon[2].attacks[2] = Attack("DYNAMICPUNCH", self.types['FIGHTING'], 50, 8, 8, False)
        self.trainerPokemon[2].attacks[3] = Attack("EXPLOSION", self.types['NORMAL'], 100, 8, 8, True)

        self.trainerPokemon[2] = self.allPokemon['FEAROW']
        self.trainerPokemon[2].attacks[0] = Attack("REST", self.types['PSYCHIC'], None, 16, 16, False)
        self.trainerPokemon[2].attacks[1] = Attack("FLY", self.types['FLYING'], 95, 15, 15, False)
        self.trainerPokemon[2].attacks[2] = Attack("DOUBLE-EDGE", self.types['NORMAL'], 100, 24, 24, True)
        self.trainerPokemon[2].attacks[3] = Attack("HIDDEN POWER", self.types['NORMAL'], 100, 24, 24, False)

        self.trainerPokemon[2] = self.allPokemon['BLASTOISE']
        self.trainerPokemon[2].attacks[0] = Attack("REST", self.types['PSYCHIC'], None, 16, 16, False)
        self.trainerPokemon[2].attacks[1] = Attack("SURF", self.types['WATER'], 100, 24, 24, False)
        self.trainerPokemon[2].attacks[2] = Attack("ZAP CANNON", self.types['ELECTRIC'], 50, 8, 8, True)
        self.trainerPokemon[2].attacks[3] = Attack("SLEEP TALK", self.types['NORMAL'], None, 16, 16, False)


    def initializeAllPokemon(self):
        res_front = resources.loadResources("resources/_POKEMON_FRONT")
        res_back = resources.loadResources("resources/_POKEMON_BACK")
        res_list =  open("resources/_pokemon_list", "r")

        if len(res_front) != len(res_back):
            print("ERROR: Not equal amount of sprites in _POKEMON_FRONT and _POKEMON_BACK")
            exit(-1)

        i = 1
        for line in iter(res_list ):
            line = line.split("\n")[0].split(" ")

            name = line[1].upper()
            type1 = self.types[line[2].upper()]
            type2 = None

            if(len(line) == 4):
                type2 = self.types[line[3].upper()]

            img_front = None
            img_back = None

            for res in res_front:
                if res.name.upper() == name:
                    img_front = res.img

            for res in res_back:
                if res.name.upper() == name:
                    img_back = res.img

            if type(img_front) is type(None) or type(img_back) is type(None):
                print("ERROR: Image for " + name + " missing!")
                exit(-1)

            p = Pokemon(name, type1, type2, img_front, img_back )
            self.allPokemon[name] = p
            i += 1
        res_list.close()

    def initializeStates(self):
        """ Hier werden die verschiedenen States, welche das Spiel haben kann initialisiert und verknüpft (entweder direkt
        im Konstruktor oder via self.states.append().
        """

        unsureState = StateHistDiff("UNSURE", 0)
        self.states.append(unsureState)

        introState = StateHistDiff( "INTRO", 0)
        self.states.append(introState)

        titleState = StateTitle("TITLE", 0)
        self.states.append(titleState)

        startMenuState = StateHistDiff("STARTMENU", 0)
        self.states.append(startMenuState)

        freeplayState = StateHistDiff("FREEPLAY", 0)
        self.states.append(freeplayState)
        startMenuState.addNextState(freeplayState)

        menuState = StateMenu("MENU", 0)
        self.states.append(menuState)
        menuState.addNextState(freeplayState)
        freeplayState.addNextState(menuState)

        optionsState = StateHistDiff("OPTIONS", 0)
        self.states.append(optionsState)

        packState = StateHistDiff( "PACK", 0)
        self.states.append(packState)

        packBallsState = StatePackBalls( "PACK_BALLS", 0)
        self.states.append(packBallsState)
        packBallsState.addNextState(packState)
        packState.addNextState(packBallsState)

        pokedexState = StateHistDiff( "POKEDEX", 0)
        self.states.append(pokedexState)

        pokemonState = StateHistDiff("POKEMON", 0)
        self.states.append(pokemonState)

        pokegearStartState = StatePokeGearStart( "POKEGEAR_START", 0)
        self.states.append(pokegearStartState)

        pokegearMapKantoState = StatePokeGearMap("POKEGEAR_MAP_KANTO", 0)
        self.states.append(pokegearMapKantoState)
        pokegearMapKantoState.addNextState(pokegearStartState)
        pokegearStartState.addNextState(pokegearMapKantoState)

        pokegearMapJohtoState = StatePokeGearMap("POKEGEAR_MAP_JOHTO", 0)
        self.states.append(pokegearMapJohtoState)
        pokegearMapJohtoState.addNextState(pokegearStartState)
        pokegearStartState.addNextState(pokegearMapJohtoState)

        pokegearPhoneState = StatePokeGearPhone("POKEGEAR_PHONE", 0)
        self.states.append(pokegearPhoneState)
        pokegearPhoneState.addNextState(pokegearMapKantoState)
        pokegearPhoneState.addNextState(pokegearMapJohtoState)
        pokegearMapKantoState.addNextState(pokegearPhoneState)
        pokegearMapJohtoState.addNextState(pokegearPhoneState)

        pokegearRadioState = StatePokeGearRadio("POKEGEAR_RADIO", 0)
        self.states.append(pokegearRadioState)
        pokegearRadioState.addNextState(pokegearPhoneState)
        pokegearPhoneState.addNextState(pokegearRadioState)

        saveState = StateHistDiff("SAVE", 0)
        self.states.append(saveState)
        saveState.addNextState([menuState, freeplayState])
        menuState.addNextState(saveState)

        trainerState = StateHistDiff("TRAINER", 0)
        self.states.append(trainerState)

        fightMenuState = StateFightMenu("FIGHT_MENU", 0, self.allPokemon)
        self.states.append(fightMenuState)


        fightAttacksState = StateFightAttacks("FIGHT_ATTACKS", 0, self.allPokemon)
        self.states.append(fightAttacksState)
        fightAttacksState.addNextState(fightMenuState)
        fightMenuState.addNextState(fightAttacksState)

        fightAttackedState = StateFightAttacked("FIGHT_ATTACKED", 0, self.allPokemon)
        self.states.append(fightAttackedState)
        fightAttackedState.addNextState(fightMenuState)
        fightAttacksState.addNextState(fightAttackedState)

        fightWonState = StateFightWon("FIGHT_WON", 0, self.allPokemon)
        self.states.append(fightWonState)
        fightAttackedState.addNextState(fightWonState)

        unsureState.addNextState([introState, titleState, startMenuState, menuState, pokedexState, pokemonState, packState, packBallsState, pokegearStartState, freeplayState, fightMenuState, fightWonState])

        introState.addNextState(unsureState)
        titleState.addNextState(unsureState)
        startMenuState.addNextState(unsureState)
        menuState.addNextState(unsureState)
        pokedexState.addNextState(unsureState)
        pokemonState.addNextState(unsureState)
        packState.addNextState(unsureState)
        packBallsState.addNextState(unsureState)
        pokegearStartState.addNextState(unsureState)
        pokegearMapKantoState.addNextState(unsureState)
        pokegearMapJohtoState.addNextState(unsureState)
        pokegearPhoneState.addNextState(unsureState)
        pokegearRadioState.addNextState(unsureState)
        freeplayState.addNextState(unsureState)
        fightMenuState.addNextState(unsureState)
        fightAttacksState.addNextState(unsureState)
        fightWonState.addNextState(unsureState)

    def initializeTypes(self):
        res = resources.loadResources("resources/_TYPES/")
        for r in res:
            t = Type(r.name, r.img)
            self.types[t.name] = t

